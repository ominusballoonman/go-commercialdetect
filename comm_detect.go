// Copyright 2011 <chaishushan@gmail.com>. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
    "fmt"
    "os"

    "code.google.com/p/go-opencv/trunk/opencv"
)

const TEST_PATH string = "/Users/berose/Development/go-CommercialDetect-media"

func main() {
    edge_thresh := 0
    filename := TEST_PATH + "/out.avi"
    if len(os.Args) == 2 {
        filename = os.Args[1]
    }

    movie := opencv.NewFileCapture(filename)
    if movie == nil {
        panic("VideoCapture fail")
    }
    defer movie.Release()

    for {
        frame := movie.QueryFrame()

        w := frame.Width()
        h := frame.Height()

        // Create the output image
        cedge := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 3)
        defer cedge.Release()

        // Convert to grayscale
        gray := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 1)
        edge := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 1)
        defer gray.Release()
        defer edge.Release()

        opencv.CvtColor(frame, gray, opencv.CV_BGR2GRAY)

        win := opencv.NewWindow("Edge")
        defer win.Destroy()

        //win.SetMouseCallback(func(event, x, y, flags int, param ...interface{}) {
        //    fmt.Printf("event = %d, x = %d, y = %d, flags = %d\n",
        //        event, x, y, flags,
        //    )
        //})

        win.CreateTrackbar("Thresh", 1, 100, func(pos int, param ...interface{}) {
            edge_thresh = pos

            fmt.Printf("pos = %d\n", pos)
        })

        opencv.Smooth(gray, edge, opencv.CV_BLUR, 3, 3, 0, 0)
        opencv.Not(gray, edge)

        // Run the edge detector on grayscale
        opencv.Canny(gray, edge, float64(edge_thresh), float64(edge_thresh*3), 3)

        opencv.Zero(cedge)
        // copy edge points
        opencv.Copy(frame, cedge, edge)

        win.ShowImage(cedge)

        key := opencv.WaitKey(20)
        if key == 27 {
            os.Exit(0)
        }
    }

    os.Exit(0)
}

